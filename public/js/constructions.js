jQuery(document).ready(function ($) {
    //Ajax settings
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //Initial state for features modal
    let featuresBeenShowed = false;
    // Main window datatable
    $('#constrTable').DataTable({
        language: {
            sProcessing: "Procesando...",
            sLengthMenu: "Mostrar _MENU_ registros",
            sZeroRecords: "No se encontraron resultados",
            sEmptyTable: "Ningún dato disponible en esta tabla",
            sInfo: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            sInfoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
            sInfoFiltered: "(filtrado de un total de _MAX_ registros)",
            sInfoPostFix: "",
            sSearch: "Buscar:",
            sUrl: "",
            sInfoThousands: ",",
            sLoadingRecords: "Cargando...",
            oPaginate: {
                sFirst: "Primero",
                sLast: "Último",
                sNext: "Siguiente",
                sPrevious: "Anterior"
            },
            oAria: {
                sSortAscending: ": Activar para ordenar la columna de manera ascendente",
                sSortDescending: ": Activar para ordenar la columna de manera descendente"
            }
        },
        columnDefs: [{
                orderable: false,
                targets: -1,
            },
            {
                orderable: false,
                targets: 0,
            }
        ]
    });
    // Validation regex method
    jQuery.validator.addMethod('regexRule', function (value, element) {
        var re = new RegExp(/^PCOM\-[a-zA-Z]{3}\/[0-9]{2}$/);
        return this.optional(element) || re.test(value);
    }, "El formato no corresponde con el establecido para el código de una construcción.");
    // Form Validation
    $('#catForm').validate({
        rules: {
            code: {
                required: true,
                regexRule: true,
            },
            name: 'required'
        }
    });
    $('#code').change(() => {
        $('#repeatedMessg').hide();
    });

    // Buttons action on construct modal
    $('#btnGuardar').click((e) => {
        e.preventDefault();
        $.ajax({
                url: '/admin/constructions/new',
                method: 'POST',
                dataType: 'json',
                data: {
                    name: $('#name').val(),
                    code: $('#code').val()
                }
            })
            .done((message) => {
                if (parseInt(message.data) == 1) {
                    window.location.reload();
                }
            })
            .fail((xhr, status, error) => {
                $('#repeatedMessg').show();

            });
    });
    //Address modal
    $('#modalConstAddrs').on("show.bs.modal", (event) => {
        let triggerBtn = $(event.relatedTarget);
        let constFullId = triggerBtn.attr('id');
        let constArrId = constFullId.split('_');
        let constID = parseInt(constArrId[1]);
        $('#tryLater').hide();
        $.ajax({
                url: '/admin/address/' + constID,
                method: 'GET',
                dataType: 'json',
            })
            .done((response) => {
                let constrCode = response.data.constrCode;
                let addressData = response.data.address;
                $('#addrConstrID').val(response.data.constrID);
                console.log($.isEmptyObject(addressData));
                if ($.isEmptyObject(addressData) == false) {
                    //Fill Data

                    $('#addressID').val(addressData.id);
                    $('#street').val(addressData.street);
                    $('#number').val(addressData.number);
                    $('#neighborhood').val(addressData.neighborhood);
                    $('#city').val(addressData.city);
                    $('#state').val(addressData.state);
                    $('#country').val(addressData.country);
                    $('#zipcode').val(addressData.zipcode);
                }
                //Fill construction code
                $('#addrssTitle').append(' ' + constrCode);
            })
            .fail((xhr, status, error) => {
                //Display error message
                $('#tryLater').show().focus().delay(5000).hide();
            });
        $('#addrrForm').validate({
            rules: {
                street: 'required',
                neighborhood: 'required',
                city: 'required',
            },
            messages: {
                street: 'Este campo es requerido',
                neighborhood: 'Este campo es requerido',
                city: 'Este campo es requerido',
            }
        });
    });
    //Validate address

    // Btn. action on address modal
    $('#btnSaveAddr').click((e) => {
        e.preventDefault();
        if ($('#addrrForm').valid() && parseInt($('#addressID').val()) == 0) {
            $.ajax({
                    url: '/admin/address/save',
                    method: 'POST',
                    dataType: 'json',
                    data: $('#addrrForm').serializeArray()
                })
                .done((response) => {
                    window.location.reload();
                })
                .fail((xhr, status, error) => {
                    $('#tryLater').show().delay(5000).hide();
                });
        } else {
            if ($('#addrrForm').valid()) {
                //Update form
                let addressID = $('#addressID').val();
                let dataArry = $('#addrrForm').serializeArray();
                dataArry.push({
                    name: 'id',
                    value: addressID
                });
                $.ajax({
                        url: '/admin/address/update',
                        method: 'POST',
                        dataType: 'json',
                        data: dataArry
                    })
                    .done((response) => {
                        window.location.reload();
                    })
                    .fail((xhr, status, error) => {
                        $('#tryLater').show().delay(5000).hide();
                    });
            }
        }
    });
    //Features modal onshow
    $('#featuresModal').on("show.bs.modal", (event) => {
        let tableRow = 2;
        let triggerBtn = $(event.relatedTarget);
        let constructID = triggerBtn.attr('data-id');

        //Load data if available.
        if (featuresBeenShowed === false) {
            $.ajax({
                    url: '/admin/feature/' + constructID,
                    method: 'GET',
                    dataType: 'json',
                })
                .done((response) => {

                    $('#constrID').val(response.constrID);
                    $('#constrTitle').append(" " + response.constrName);
                    let features = response.features;

                    if ($.isEmptyObject(features) == false) {
                        console.log("A llenar datos");
                        let featuresHtml = '';
                        $('#featuresTbody').html(featuresHtml);
                        $.each(features, (index, value) => {
                            featuresHtml += '<tr class="featureReg">';
                            featuresHtml += '<td>' + value.id + '</td>';
                            featuresHtml += '<td>' + value.feature + '</td>';
                            featuresHtml += '<td>' + value.featureDescription + '</td>';
                            featuresHtml += '</tr>';
                        });
                        $('#featuresTbody').html(featuresHtml);
                        $('#featuresTable').SetEditable({
                            $addButton: $('#but_add'),
                            columnsEd: "2,3", // Ex.: "1,2,3,4,5"
                        });
                    }
                    featuresBeenShowed = true;
                });

        }
        $('#btnFeatGuardar').click((event) => {
            event.preventDefault();
            let dataSet = [];
            $('.featureReg').each((index, value) => {
                dataSet.push({
                    feature: $(value).children('td').eq(1).html(),
                    featureDescription: $(value).children('td').eq(2).html(),
                });
            });
            let featuresRecords = {
                constr_id: $('#constrID').val(),
                featuresDataSet: dataSet,
            };
            $.ajax({
                url:'/admin/features/saveAll',
                method: 'POST',
                dataType: 'json',
                data: featuresRecords
            }).done((response)=>{
                if(parseInt(response)==1){
                    window.location.reload();
                }
            });
        });
    });

});
