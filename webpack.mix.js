const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.copyDirectory('resources/js/', 'public/js')
    .copyDirectory('node_modules/font-awesome/fonts/', 'public/fonts')
    .copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts')
    .copy('resources/img/', 'public/img')
    .copy('node_modules/admin-lte/dist', 'public/adminlte')
    .copy('node_modules/admin-lte/plugins', 'public/adminlte/plugins')
    .copy('node_modules/@fortawesome/fontawesome-free/css/all.css', 'public/css/')
    .copy('node_modules/ionicons/dist/css/ionicons-core.min.css', 'public/css/')
    .copy('node_modules/ionicons/dist/css/ionicons.min.css', 'public/css/')
    .copy('node_modules/jquery-validation/dist/jquery.validate.min.js', 'public/js')
    .copy('node_modules/jquery-validation/dist/additional-methods.min.js', 'public/js')
    .copy('node_modules/jquery-validation/dist/localization/messages_es.js', 'public/js/localization')
    .sass('resources/sass/styles.scss', 'public/css/styles.css')
    .sass('resources/sass/backend.scss', 'public/css/backend.css')
    .sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false
    })
    .browserSync({
        'proxy': 'promociones.test',
        'cors': true,
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
        }
    });
