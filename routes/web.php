<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Admin routes
Route::get('/admin/constructions', 'BackEnd\ConstructionController@index')->name('home');
// Constructions
// Route::get('/admin/constructions', 'BackEnd\ConstructionController@index');
Route::post('/admin/constructions/new', 'BackEnd\ConstructionController@store');

//Address
Route::get('/admin/address/{id}', 'BackEnd\AddressController@getAddress');
Route::post('/admin/address/save', 'BackEnd\AddressController@store');
Route::post('/admin/address/update','BackEnd\AddressController@update');
//Features
Route::get('/admin/feature/{id}', 'BackEnd\FeatureController@getFeatures');
Route::post('/admin/features/saveAll','BackEnd\FeatureController@saveAll');
//FourSquare
Route::get('/admin/foursquare/venues/{latitude}/{longitude}', 'BackEnd\FoursquareController@fakeUrls');


//Logout
Route::get('/admin/logout', function (){
    Auth::logout();
    return redirect('/');
});
