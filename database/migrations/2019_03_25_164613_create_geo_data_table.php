<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('construction_id');
            $table->decimal('latitutede',10,5);
            $table->decimal('longitude',10,5);
            $table->timestamps();
            $table->softDeletes();
            //Foreign keys
            $table->foreign('construction_id')->references('id')->on('constructions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_data');
    }
}
