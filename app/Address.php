<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //Fillable fields
    protected $fillable =[
        'construction_id','street','number','neighborhood','city','state','country','zipcode'
    ];
    //Relations
    /**
     * Returns a collection object with the related construction
     *
     * @return laravel's collection object
     */
    public function construction(){
        return $this->belongsTo('App\Construction');
    }
}
