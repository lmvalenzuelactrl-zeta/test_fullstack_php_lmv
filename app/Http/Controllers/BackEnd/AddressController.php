<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddressRequest;
use App\Construction;
use App\Address;

class AddressController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');

    }
    public function getAddress($id){
        $construction = Construction::find($id);
        $data = [
            'constrCode'    =>  $construction->code,
            'constrID'      =>  $construction->id,
            'address'       =>  $construction->address !==NULL ? $construction->address : []
        ];
        return response()->json([
            'data'  =>  $data
        ],200);
    }
    public function store(AddressRequest $request){
        // print_r($request->input());
        $constAddress= Address::create($request->input());
        if($constAddress){
            return response('1',200);
        }
    }
    public function update(AddressRequest $request){
        $addressSaved = Address::find($request->input('id'))->update($request->input());
        if($addressSaved){
            return response('1',200);
        } else {
            return response('error', 404);
        }
    }
}
