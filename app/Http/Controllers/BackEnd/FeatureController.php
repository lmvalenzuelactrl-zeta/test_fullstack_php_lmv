<?php

namespace App\Http\Controllers\BackEnd;

use App\Construction;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Feature;

class FeatureController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');

    }
    public function getFeatures($id)
    {
        $construction = Construction::find($id);
        $data         = [
            'constrID'   => $construction->id,
            'constrName' => $construction->code,
            'features'   => $construction->features,
        ];
        return response()->json($data, 200);
    }
    public function saveAll(Request $request)
    {
        /*
        {constr_id: "4", featuresDataSet: Array(1)}
        constr_id: "4"
        featuresDataSet: Array(1)
        0:
        feature: "Esta"
        featureDescription: "es una prueba"
         */
        $fullDataSet  = $request->input();
        $isNewDataSet = DB::table('features')
            ->where('construction_id', $fullDataSet['constr_id'])
            ->count();
        if ($isNewDataSet > 0) {
            DB::table('features')->where('construction_id', $fullDataSet['constr_id'])
                ->delete();
        }
        try {
            foreach ($fullDataSet['featuresDataSet'] as $feature) {
                $replaceData = [
                    'construction_id'    => $fullDataSet['constr_id'],
                    'feature'            => $feature['feature'],
                    'featureDescription' => $feature['featureDescription'],
                ];
                Feature::create($replaceData);
            }
            return response(1,200);
        } catch (\Illuminate\Database\QueryException $exception) {
            return $response()->json([
                'error' =>  $exception->errorInfo
            ], 400);
        }

    }
}
