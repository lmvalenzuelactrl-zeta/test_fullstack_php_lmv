<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FoursquareController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getVenues($latitude, $longitude)
    {
        $arrayPhotos = [];
        $client      = new Client();
        $result      = $client->request(
            'GET',
            'https://api.foursquare.com/v2/venues/search',
            [
                'query' => [
                    'll'            => $latitude . "," . $longitude,
                    'client_id'     => env('FOUR_SQUARE_ID'),
                    'client_secret' => env('FOUR_SQUARE_SECRET'),
                    'v'             => env('FOUR_SQUARE_VERSION'),
                    'limit'         => 5,
                ],
            ]
        );
        $response   = $result->getBody()->getContents();
        $collection = collect(json_decode($response, true));
        $idx        = 0;
        foreach ($collection['response']['venues'] as $venues) {
            $photoResult = $client->request(
                'GET',
                'https://api.foursquare.com/v2/venues/' . $venues['id'] . '/photos',
                [
                    'query' => [
                        'client_id'     => env('FOUR_SQUARE_ID'),
                        'client_secret' => env('FOUR_SQUARE_SECRET'),
                        'v'             => env('FOUR_SQUARE_VERSION'),
                        'limit'         => 1,
                    ],
                ]
            );
            $photoResponse   = $photoResult->getBody()->getContents();
            $photoCollection = collect(json_decode($photoResponse, true));
            foreach ($photoCollection['response']['photos'] as $photo) {

                sprintf('<pre>%s</pre>', print_r($photo['items']), true);
                echo "<br />";
                // echo $photo['prefix']."width100".$photo['suffix']."<br />";
            }
            $idx = $idx + 1;
        }
    }

    public function fakeUrls()
    {
        $urlArry = [
            [
                'venue_id' => '4e8bcf9c0aaf7fe613bec7a1',
                'url'      => 'https://fastly.4sqi.net/img/general/width100/30150847_zYPBnJf0xxOqmDS_KGppLI7RoccSg1zZ7SaQqMmDZ-4.jpg',
            ],
            [
                'venue_id' => '4e8bcf9c0aaf7fe613bec7a1',
                'url'      => 'https://fastly.4sqi.net/img/general/width100/VNL3MBB4O1MAQLU3CUVFLIXEOMYTAKI5ATOQDLHAYOJUVSRZ.jpg',
            ],
            [
                'venue_id' => '4e8bcf9c0aaf7fe613bec7a1',
                'url'      => 'https://fastly.4sqi.net/img/general/width100/41902134_AbiaywXU2ESBlzrvWXrgiwlX7eB-m-t5-kp9cAkbl34.jpg',
            ],
            [
                'venue_id' => '4e8bcf9c0aaf7fe613bec7a1',
                'url'      => 'https://fastly.4sqi.net/img/general/width100/54407702_22_euLttdeW1XoQZn0k8n_n4a5wgj1t0zwqr-1_Y884.jpg',
            ],
            [
                'venue_id' => '4e8bcf9c0aaf7fe613bec7a1',
                'url'      => 'https://fastly.4sqi.net/img/general/width100/22666568_XQY82kk18ht539sQF9Ev1XJvSqUbXw-m8yNyPkmfesA.jpg',
            ],
        ];
        return response()->json($urlArry, 200);
    }
}
