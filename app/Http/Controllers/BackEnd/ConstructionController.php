<?php

namespace App\Http\Controllers\BackEnd;

use App\Construction;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConstructionRequest;

class ConstructionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    //
    public function index()
    {
        $data = [
            'constructions' =>  Construction::all()
        ];
        return view('backend.constructions', $data);
        // print_r(Auth::user());

    }
    public function store(ConstructionRequest $request)
    {
        $constrData         = $request->validated();
        $construction       = new Construction();
        $construction->name = $constrData['name'];
        $construction->code = $constrData['code'];
        try {
            $construction->save();
            return response()->json([
                'data' => 1,
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            //Houston we have a duplicate
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                return response([
                    'errors'=>'Duplicate Entry'
                ], 400);
            }
        }
    }
}
