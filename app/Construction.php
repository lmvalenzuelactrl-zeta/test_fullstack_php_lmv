<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Construction extends Model
{
    use SoftDeletes;
    //
    protected $fillable = [
        'code', 'name',
    ];
    //Relations
    /**
     *  Returns a collection object with the related address
     *
     * @return laravel's collection
     */
    public function address()
    {
        return $this->hasOne('App\Address');
    }
    /**
     * Returns a collection object with all the related features
     *
     * @return laravel's collection object
     */
    public function features()
    {
        return $this->hasMany('App\Feature');

    }
    public function foursquares(){
        return $this->hasMany('App\Foursquare');
    }
}
