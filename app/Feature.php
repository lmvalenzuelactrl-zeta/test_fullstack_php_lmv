<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    // Fillable
    protected $fillable =[
        'construction_id','feature','featureDescription'
    ];
    // Relations
    public function construction() {
        return $this->belongsTo('App\Construction');
    }
}
