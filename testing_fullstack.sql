﻿# Host: 127.0.0.1  (Version 5.7.24)
# Date: 2019-03-25 14:15:55
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "constructions"
#

DROP TABLE IF EXISTS `constructions`;
CREATE TABLE `constructions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `constructions_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "constructions"
#

INSERT INTO `constructions` VALUES (4,'PCOM-adf/12','Prueba','2019-03-24 01:19:44','2019-03-24 01:19:44',NULL),(13,'PCOM-adf/14','Prueba','2019-03-24 02:59:42','2019-03-24 02:59:42',NULL);

#
# Structure for table "addresses"
#

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `construction_id` bigint(20) unsigned NOT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `neighborhood` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zipcode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_construction_id_foreign` (`construction_id`),
  CONSTRAINT `addresses_construction_id_foreign` FOREIGN KEY (`construction_id`) REFERENCES `constructions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "addresses"
#

INSERT INTO `addresses` VALUES (1,4,'Esta','es','una','prueba','de','mi','gran genio','2019-03-24 07:32:19','2019-03-24 17:04:18',NULL),(4,4,'Est','es','una','pruebas','de','mi','gran genio','2019-03-24 16:41:34','2019-03-24 16:41:34',NULL);

#
# Structure for table "features"
#

DROP TABLE IF EXISTS `features`;
CREATE TABLE `features` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `construction_id` bigint(20) unsigned NOT NULL,
  `feature` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featureDescription` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `features_construction_id_foreign` (`construction_id`),
  CONSTRAINT `features_construction_id_foreign` FOREIGN KEY (`construction_id`) REFERENCES `constructions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "features"
#

INSERT INTO `features` VALUES (3,4,'Esta','es una prueba','2019-03-25 07:49:43','2019-03-25 07:49:43',NULL),(4,4,'Esta','Es otra prueba','2019-03-25 07:49:43','2019-03-25 07:49:43',NULL);

#
# Structure for table "geo_data"
#

DROP TABLE IF EXISTS `geo_data`;
CREATE TABLE `geo_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `construction_id` bigint(20) unsigned NOT NULL,
  `latitutede` decimal(10,5) NOT NULL,
  `longitude` decimal(10,5) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `geo_data_construction_id_foreign` (`construction_id`),
  CONSTRAINT `geo_data_construction_id_foreign` FOREIGN KEY (`construction_id`) REFERENCES `constructions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "geo_data"
#


#
# Structure for table "images"
#

DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `construction_id` bigint(20) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `images_construction_id_foreign` (`construction_id`),
  CONSTRAINT `images_construction_id_foreign` FOREIGN KEY (`construction_id`) REFERENCES `constructions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "images"
#


#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_03_22_214112_create_constructions_table',2),(5,'2019_03_23_005239_create_addresses_table',3),(6,'2019_03_23_005324_create_features_table',3),(8,'2019_03_25_164420_create_images_table',4),(9,'2019_03_25_164613_create_geo_data_table',5);

#
# Structure for table "password_resets"
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "password_resets"
#


#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (2,'Luis Miguel','Valenzuela Arteaga','lmvalenzuela@ctrl-zeta.com',NULL,'$2y$10$KrtBZdRcJcrCWoIFiUdxG.wPWwF3f8glZWlqOcCXAKLBy5Qgng0gC',NULL,'2019-03-22 21:38:53','2019-03-22 21:38:53',NULL);
