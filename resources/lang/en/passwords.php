<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben ser al menos de 8 caracteres y coincidir con el campo Confirme Contraseña.',
    'reset' => 'Su contraseña ha sido restablecida.',
    'sent' => 'Hemos mandado a la dirección registrada de correo electrónico un mensaje con una liga para restablecer su contraseña.',
    'token' => 'El token relacionado con la contraseña es inválido.',
    'user' => "No hemos podido encontrar un usuario con ese correo electrónico.",

];
