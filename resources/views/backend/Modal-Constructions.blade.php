<div class="modal" id="constructionsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Construcción</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title">Detalle de la construcción</h3>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form class="form" id="catForm" role="form">
                            <div class="form-group">
                                <input type="hidden" name="constructionID" id="constructionID" value="0" />
                                <label for="code">Clave de la construcción</label>
                                <input type="text" class="form-control" name="code" id="code" placeholder="PCOM-XXX/##"
                                required/>
                            </div>
                            <div class="form-group">
                                <label for="name">Nombre:</label>
                                <input type="text" class="form-control" name="name" id="name" required />
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="modal-footer">
                <p class="danger" id="repeatedMessg">Revise la clave de la construcción porque la clave <strong id="repeteadKey"></strong> ya existe</p>
                <button type="button" class="btn btn-primary" id="btnGuardar">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
