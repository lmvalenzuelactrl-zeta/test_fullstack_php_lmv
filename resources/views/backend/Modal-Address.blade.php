
<div class="modal" id="modalConstAddrs" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Dirección</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title" id="addrssTitle">Clave construcción:</h3>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form class="form" id="addrrForm" role="form">
                            <div class="form-group">
                                <input type="hidden" name="construction_id" id="addrConstrID" value="0" />
                                <label for="street">Calle</label>
                                <input type="text" class="form-control" name="street" id="street"
                                required/>
                            </div>
                            <div class="form-group">
                                <label for="number">Número:</label>
                                <input type="text" class="form-control" name="number" id="number"/>
                            </div>
                            <div class="form-group">
                                <label for="neighborhood">Colonia:</label>
                                <input type="text" class="form-control" name="neighborhood" id="neighborhood" required/>
                            </div>
                            <div class="form-group">
                                <label for="city">Delegación/Municipio:</label>
                                <input type="text" class="form-control" name="city" id="city"/>
                            </div>
                            <div class="form-group">
                                <label for="state">Estado:</label>
                                <input type="text" class="form-control" name="state" id="state"/>
                            </div>
                            <div class="form-group">
                                <label for="country">País:</label>
                                <input type="text" class="form-control" name="country" id="country"/>
                            </div>
                            <div class="form-group">
                                <label for="zipcode">Código postal:</label>
                                <input type="zip" class="form-control" name="zipcode" id="zipcode"/>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="modal-footer">
                <p class="text-danger" id="tryLater">Ha ocurrido un error. Espere unos segundos e intente de nuevo.</p>
                <input type="hidden" name="addressID" id="addressID" value="0"/>
                <button type="button" class="btn btn-primary" id="btnSaveAddr">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
