@extends('layouts.backendbase')
@section('additional-css')
<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/select2.css') }}">
@endsection

@section('content')
    @include('backend.Modal-Constructions')
    @include('backend.Modal-Address')
    @include('backend.Modal-Features')

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Listado de Construcciones</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/home') }}">Home</a></li>
                        <li class="breadcrumb-item active">Construccciones</li>
                    </ol>
                </div>
            </div>
        </div>
        <!--/container-fluid-->
    </section>
    <section class="content">
        <div class="card">
            <div class="card-header bg-primary-gradient">
                <nav class="navbar navbar-light bg-light">
                    <a class="navbar-brand"><i class="fas fa-hard-hat"></i> Construcción</a>
                    <form class="form-inline" id="navForm">
                        <button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#constructionsModal"><i class="fas fa-plus-circle"></i> Nueva</button>
                    </form>
                </nav>
            </div>
            <div class="card-body">
                <table id="constrTable" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Clave</th>
                            <th>Nombre</th>
                            <th>Dirección</th>
                            <th>Operaciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($constructions as $construction)
                        <tr>
                            <td>{{ $construction->id }}</td>
                            <td>{{ $construction->code }}</td>
                            <td>{{ $construction->name }}</td>
                            <td>{{ $construction->address !== NULL ? $construction->address->street." ".$construction->address->neighborhood." ".$construction->address->city: 'No hay un registro aún' }}</td>
                            <td>
                                <div class="btn-group btn-group-sm">
                                    <button class="btn btn-primary btn-sm" id="btnAddrs_{{ $construction->id }}" data-toggle="modal" data-target="#modalConstAddrs"
                                        data-id="{{ $construction->id }}"><i class="fas fa-map-marker-alt"></i> Dirección</button>
                                    <button class="btn btn-secondary btn-sm" id="btnFeat_{{ $construction->id }}" data-toggle="modal" data-target="#featuresModal"
                                        data-id="{{ $construction->id }}"><i class="fas fa-clipboard-list"></i> Características</button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('js/bootstable.js') }}"></script>
    <script src="{{ asset('js/constructions.js')}}"></script>
@endsection
