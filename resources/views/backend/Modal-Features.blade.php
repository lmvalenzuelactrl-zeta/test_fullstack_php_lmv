<div class="modal" id="featuresModal" tabindex="-1" role="dialog">
    <div class="modal-dialog mw-100 w-75" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Construcción</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title" id="constrTitle">Características de la construcción</h3>
                        <!-- /.card-tools -->
                        <div class="col-12">
                            <span>
                              <button id="but_add" class="btn btn-sm btn-primary"><i class="fas fa-plus-circle"></i> Agregar</button>
                            </span>
                        </div>
                    </div>

                    <!-- /.card-header -->
                    <div class="card-body">

                        <table id="featuresTable" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Característica</th>
                                    <th>Descripción larga</th>
                                </tr>
                            </thead>
                            <tbody id="featuresTbody">

                            </tbody>
                        </table>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="modal-footer">
                <input type="hidden" id="constrID" value="0" />
                <button type="button" class="btn btn-primary" id="btnFeatGuardar">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
